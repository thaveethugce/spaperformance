var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
export var AppComponent = (function () {
    function AppComponent() {
        this.name = 'Angular';
        this.title = 'my test prj';
        this.heros = msgs;
    }
    AppComponent.prototype.updateC = function () {
        var msgs2 = [];
        for (var i = 0; i < 3500; i++) {
            msgs2[i] = {};
            for (var j = 0; j < 5; j++) {
                if (j == 0 || (i % 5 == 0 && j == 3)) {
                    msgs2[i][j] = Math.random();
                }
                else {
                    msgs2[i][j] = this.heros[i][j];
                }
            }
        }
        this.heros = msgs2;
    };
    AppComponent = __decorate([
        Component({
            selector: 'my-app',
            template: "\n    <h2>My favorite hero is   jhhh: {{title}}</h2>\n    <a href=\"#\" (click)=\"updateC()\">re render</a>\n     <ul>\n      <li *ngFor=\"let user of heros\">\n     <span style=\"margin: 20px\">{{user['0']}}</span>\n    <span style=\"margin: 20px\">{{user['1']}}</span>\n    <span style=\"margin: 20px\">{{user['2']}}</span>\n     <span style=\"margin: 20px\">{{user['3']}}</span>\n    <span style=\"margin: 20px\">{{user['4']}}</span>\n      </li>\n    </ul>\n\n  ",
        }), 
        __metadata('design:paramtypes', [])
    ], AppComponent);
    return AppComponent;
}());
var msgs = [];
for (var i = 0; i < 3500; i++) {
    msgs[i] = {};
    for (var j = 0; j < 5; j++) {
        msgs[i][j] = Math.random();
    }
}
//# sourceMappingURL=app.component.js.map